from difflib import SequenceMatcher as Sm
from bs4 import BeautifulSoup as Bs
from dataclasses import dataclass
import requests as rs
import re

class Crawse:
    def __init__(
        self, 
        url: str = None
    ) -> None:
        self.url  = url
        if self.url:
            self.r    = rs.get(self.url)
            self.soup = Bs(self.r.content, 'html.parser')
        else:
            self.r    = None
            self.soup = None

    def set_page(self, url: str) -> None:
        self.url  = url
        self.r    = rs.get(self.url)
        self.soup = Bs(self.r.content, 'html.parser')

    def tin(self, i: str) -> dict:
        t = ''
        for c in i:
            if t == 'set page ':
                return {'action': 'set_page', 'target': i[9:]}
            elif t == 'search ':
                return {'action': 'search', 'target': i[7:]}
            else:
                t += c.lower()
        return {'action': None}

    def response(self, i: str) -> str:
        tin = self.tin(i)
        if tin['action'] == 'set_page':
            try:
                self.set_page(tin['target'])
            except Exception as e:
                print('Got a error:', str(e))
            else:
                print("Current Page:", tin['target'])
        elif tin['action'] == 'search':
            if self.url:
                ratings = {}
                results = []
                for link in self.links:
                    ratings[link] = Sm(
                        None,
                        link,
                        tin['target']
                    ).ratio()
                for v in sorted(list(ratings.values()), reverse=True)[:11]:
                    results.append(list(ratings.keys())[list(ratings.values()).index(v)])
                print('Results:\n' + '\n'.join(results))
            else:
                print("No pages specified.")
    @property
    def content(self):
        if self.url:
            return self.soup.prettify
        return ''
    
    @property
    def links(self):
        if self.url:
            return [a.get('href') for a in self.soup.find_all('a', href=True)]
        return ''

cs = Crawse()
while 1:
    i = str(input())
    cs.response(i)